import numpy as np
import time

#умножение здорового человека
def matrix_multiplication(data1, data2):
    start_time = time.time()
    data1*data2
    return time.time() - start_time

#функциональненькое умножение, zip позволяет бегать двумя элементами по двум массивам сразу
def for_zip_multiplication(N, data1, data2):
    start_time = time.time()
    if (N == 1):
        for i, j in zip(data1, data2):
            i*j
    elif (N == 2):
        for i, j in zip(data1, data2):
            for m, n in zip(i, j):
                m*n
    else:
        for i, j in zip(data1, data2):
            for m, n in zip(i, j):
                for k, l in zip(m, n):
                    m*n
    return time.time() - start_time

#родной, привычный, медленный и неудобный for
def for_multiplication(N, data1, data2):
    start_time = time.time()
    if (N == 1):
        for i, dat in enumerate(data1):
            dat*data2[i]
    elif (N == 2):
        for i, dat in enumerate(data1):
            for j, da in enumerate(dat):
                da*data2[i,j]
    else:
        for i, dat in enumerate(data1):
            for j, da in enumerate(dat):
                for k, d in enumerate(da):
                    d*data2[i,j,k]
    return time.time() - start_time
