from messures import *
import matplotlib.pyplot as plt

#проведем замеры времени для матриц размерности 1, 2 и 3
for N in range(1,4):
    plt.figure()

    #получаем результаты из методов замера времени
    n_for, y_for, err_for = messure_for(N)
    n_np, y_np, err_np = messure_np(N)
    n_zip, y_zip, err_zip = messure_for_zip(N)

    #строим 
    plt.errorbar(n_for, y_for, yerr=err_for, ls='None', color = "red", capsize=8)
    plt.plot(n_for, y_for, color = "red", label = "for")
    
    plt.errorbar(n_for, y_np, yerr=err_np, ls='None', color = "blue", capsize=8)
    plt.plot(n_np, y_np, color = "blue", label = "numpy")

    plt.errorbar(n_zip, y_zip, yerr=err_zip, ls='None', color = "green", capsize=8)
    plt.plot(n_zip, y_zip, color = "green", label = "zip")

    plt.legend()
    plt.show()
    plt.savefig('dim' + str(N))
